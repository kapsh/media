# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=SDL2_image-${PV}
require SDL_lib

SUMMARY="Image file loading library for SDL"

LICENCES="ZLIB"
SLOT="$(ever range 1)"
PLATFORMS="~amd64 ~armv7 ~x86"
MYOPTIONS="
    tiff
    webp [[ description = [ Support for the WebP image format ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        media-libs/SDL:2[>=2.0.8]
        media-libs/libpng:=
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        tiff? ( media-libs/tiff )
        webp? ( media-libs/libwebp:= )
"

WORK="${WORKBASE}/${MY_PNV}"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # link to the graphics libraries instead of dlopening them at runtime
    --disable-jpg-shared
    --disable-png-shared
    --disable-tif-shared
    --disable-webp-shared
    --enable-jpg
    --enable-png
    --enable-svg
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'tiff tif'
    webp
)

