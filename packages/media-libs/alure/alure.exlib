# Copyright 2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake

export_exlib_phases src_prepare

SUMMARY="A utility library to help manage common tasks with OpenAL applications"
DESCRIPTION="
This includes device enumeration and initialization, file loading, and
streaming. The purpose of this library is to provide pre-made functionality
that would otherwise be repetitive or difficult to (re)code for various
projects and platforms, such as loading a sound file into an OpenAL buffer and
streaming an audio file through a buffer queue. Support for different formats
is consistant across platforms, so no special checks are needed when loading
files, and all formats are handled through the same API.

Currently ALURE includes a basic .wav and .aif file reader, and can leverage
external libraries such as libSndFile, VorbisFile , FLAC and others."

HOMEPAGE="http://kcat.strangesoft.net/alure.html"
DOWNLOADS="http://kcat.strangesoft.net/alure-releases/${PNV}.tar.bz2"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    examples flac
    fluidsynth [[ description = [ Support MIDI synthesizing with fluidsynth ] ]]
    modplug    [[ description = [ Support for MOD-like Music formats ] ]]
    mpg123     [[ description = [ Support for MP1/MP2/MP3 through mpg123 ] ]]
    sndfile    [[ description = [ Support for reading and writing audio data using libsndfile ] ]]
    vorbis
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/openal
        flac? ( media-libs/flac )
        fluidsynth? ( media-sound/fluidsynth[>=1.1.1] )
        modplug? ( media-libs/libmodplug )
        mpg123? ( media-sound/mpg123 )
        sndfile? ( media-libs/libsndfile )
        vorbis? ( media-libs/libvorbis )
"
# NOTE: It can rebuild its docs with NaturalDocs, which is buried. Left out
# the dep to avoid annoying people with the 'doc' option globally enabled.

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED:BOOL=TRUE
    -DBUILD_STATIC:BOOL=FALSE
    -DDYNLOAD:BOOL=FALSE
    -DDUMB:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=( EXAMPLES )
CMAKE_SRC_CONFIGURE_OPTIONS=(
    FLAC
    FLUIDSYNTH
    MODPLUG
    MPG123
    SNDFILE
    VORBIS
)

alure_src_prepare() {
    cmake_src_prepare

    # Install docs to the right place
    edo sed -e "/DESTINATION /s:share/doc/alure:/usr/share/doc/${PNVR}:" \
            -i CMakeLists.txt
}

