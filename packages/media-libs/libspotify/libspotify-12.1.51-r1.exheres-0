# Copyright 2012-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="C API package that allows to utilize the Spotify music streaming service"
HOMEPAGE="https://mopidy.github.io/${PN}-archive"
DOWNLOADS="
listed-only:
    https://mopidy.github.io/${PN}-archive/${PNV}-Linux-x86_64-release.tar.gz
    https://mopidy.github.io/${PN}-archive/${PNV}-Linux-i686-release.tar.gz
"

LICENCES="
    libspotify
    Boost-1.0 [[ note = [ Third-party license Boost ] ]]
    MIT [[ note = [ Third-party license Expat ] ]]
    CPOL [[ note = [ Third-party license FastDelegate ] ]]
    BSD-3 [[ note = [ Third-party license lib{ogg,vorbis}, Mersenne Twister ] ]]
    as-is [[ note = [ Third-party license zlib ] ]]
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES=""

WORK="${WORKBASE}"

src_compile() {
 :
}

src_install() {
    local arch
    target=$(exhost --target)
    arch=${PNV}-Linux-${target%%-*}-release

    dodir /usr/${target}/include/libspotify
    insinto /usr/${target}/include/libspotify
    doins -r ${arch}/include/libspotify/*

    dolib ${arch}/lib/libspotify*

    edo sed \
        -e "s:PKG_PREFIX:/usr/${target}:g" \
        -e 's:${prefix}/include:${prefix}/include/libspotify:g' \
        -i ${arch}/lib/pkgconfig/libspotify.pc

    dodir /usr/${target}/lib/pkgconfig
    insinto /usr/${target}/lib/pkgconfig
    doins -r ${arch}/lib/pkgconfig/*

    doman ${arch}/share/man3/*

    insinto /usr/share/doc/${PNVR}
    doins -r ${arch}/share/doc/libspotify/*
}

