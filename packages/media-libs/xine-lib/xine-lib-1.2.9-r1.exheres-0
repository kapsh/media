# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ project=xine suffix=tar.xz ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]

SUMMARY="Core library for the Xine media player"
HOMEPAGE="https://www.xine-project.org/"

LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    a52
    aac
    aalib
    bluray  [[ description = [ Support for reading Blu-ray discs via libbluray ] ]]
    caca    [[ description = [ Enable the CACA (coloured ascii) video mode ] ]]
    dts
    dvb
    dxr3
    flac
    imagemagick
    jack
    mp3     [[ description = [ Support for mp3 decoding with mad ] ]]
    musepack
    opengl
    pulseaudio
    sdl
    speex   [[ requires = vorbis ]]
    theora
    truetype
    v4l
    vcd
    vidix   [[ description = [ Support for vidix video output ] ]]
    vaapi   [[ description = [ Offload parts of video encoding to the GPU using VAAPI ] ]]
    vdpau   [[ description = [ Offload parts of video encoding to the GPU using VDPAU ] ]]
    vorbis
    vpx     [[ description = [ Enable VP8 codec support ] ]]
    wavpack [[ description = [ Build with WavPack support ] ]]
    xvmc    [[ description = [ Support for XVideo Motion Compensation (accelerated mpeg playback) ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media/ffmpeg[>=2.2.0]
        media-libs/libdvdnav[>=0.1.9]
        media-libs/libmng[lcms]
        sys-sound/alsa-lib[>=0.9.0]
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXinerama
        x11-libs/libXv [[ note = [ Automagic dep. All x11-libs deps can be made optional if this is fixed ] ]]
        x11-libs/libxcb
        a52? ( media-libs/a52dec[>=0.7.4] )
        aac? ( media-libs/faad2 )
        aalib? ( media-libs/aalib )
        bluray? ( media-libs/libbluray[>=0.2.1] )
        caca? ( media-libs/libcaca[>=0.99_beta14] )
        dts? ( media-libs/libdca )
        flac? ( media-libs/flac )
        imagemagick? ( media-gfx/ImageMagick )
        jack? ( media-sound/jack-audio-connection-kit[>=0.100] )
        mp3? ( media-libs/libmad )
        musepack? ( media-libs/libmpcdec )
        opengl? (
            x11-dri/freeglut
            x11-dri/mesa
        )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.7] )
        sdl? ( media-libs/SDL )
        speex? ( media-libs/speex )
        theora? ( media-libs/libtheora )
        truetype? (
            media-libs/fontconfig
            media-libs/freetype:2
        )
        v4l? ( media-libs/v4l-utils )
        vcd? (
            dev-libs/libcdio[>=0.71]
            media-libs/vcdimager[>=0.7.23]
        )
        vdpau? ( x11-libs/libvdpau )
        vaapi? ( x11-libs/libva )
        vorbis? (
            media-libs/libogg
            media-libs/libvorbis
        )
        vpx? ( media-libs/libvpx:= )
        wavpack? ( media-sound/wavpack )
        xvmc? ( x11-libs/libXvMC )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.2.x-libv4l.patch
    "${FILES}"/${PN}-1.2.8-openhevc.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-antialiasing
    --enable-asf
    --enable-avformat
    --enable-ipv6
    --enable-libxine-builtins
    --enable-mmap
    --enable-mng
    --enable-xinerama
    --disable-gdkpixbuf
    --disable-gnomevfs
    --disable-modplug
    --disable-oss
    --disable-real-codecs
    --disable-samba
    --with-alsa
    # Use external dvdnav library (not recommended)
    --with-external-dvdnav
    --with-x
    --with-xcb
    --without-esound
    --without-fusionsound
    --without-libstk
    --without-openhevc
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "a52 a52dec"
    "aac faad"
    aalib
    bluray
    dts
    dvb
    dxr3
    "mp3 mad"
    musepack
    opengl
    "opengl glu"
    v4l
    "v4l libv4l"
    "v4l v4l2"
    vcd
    vdpau
    vidix
    vpx
    xvmc
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    caca
    "flac libflac"
    imagemagick
    jack
    pulseaudio
    sdl
    speex
    theora
    "truetype freetype"
    "truetype fontconfig"
    vorbis
    wavpack
)

src_prepare() {
    option imagemagick \
        && has_version 'media-gfx/ImageMagick[>=7]' \
        && expatch "${FILES}"/${PN}-1.2.6-imagemagick7.patch

    autotools_src_prepare
}

