# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2016-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

DOWNLOADS_BASE="https://fpdownload.adobe.com/pub/flashplayer/pdc/${PV}"
DOWNLOADS_NPAPI_AMD64="flash_player_npapi_linux.x86_64.tar.gz"
DOWNLOADS_NPAPI_X86="flash_player_npapi_linux.i386.tar.gz"
DOWNLOADS_PPAPI_AMD64="flash_player_ppapi_linux.x86_64.tar.gz"
DOWNLOADS_PPAPI_X86="flash_player_ppapi_linux.i386.tar.gz"

require freedesktop-desktop gtk-icon-cache

SUMMARY="Adobe Flash Player"
HOMEPAGE="https://labs.adobe.com/downloads/flashplayer.html"
DOWNLOADS="
    listed-only:
        platform:amd64? (
            ${DOWNLOADS_BASE}/${DOWNLOADS_NPAPI_AMD64} -> ${PNV}-npapi.x86_64.tar.gz
            ${DOWNLOADS_BASE}/${DOWNLOADS_PPAPI_AMD64} -> ${PNV}-ppapi.x86_64.tar.gz
        )
        platform:x86? (
            ${DOWNLOADS_BASE}/${DOWNLOADS_NPAPI_X86} -> ${PNV}-npapi.i386.tar.gz
            ${DOWNLOADS_BASE}/${DOWNLOADS_PPAPI_X86} -> ${PNV}-ppapi.i386.tar.gz
        )
"

LICENCES="AdobeFlash"
SLOT="0"
PLATFORMS="-* ~amd64 ~x86"
MYOPTIONS="platform: amd64 x86"

RESTRICT="strip"

DEPENDENCIES="
    run:
        dev-libs/atk
        dev-libs/glib:2
        dev-libs/nspr
        dev-libs/nss
        media-libs/fontconfig
        media-libs/freetype:2
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:2
        x11-libs/libX11
        x11-libs/libXcursor
        x11-libs/libXext
        x11-libs/libXrender
        x11-libs/libXt
        x11-libs/pango
    build+run:
        !net-www/chromium-stable-flash-plugin [[
            description = [ adobe-flash now provides the PPAPI flash plugin ]
            resolution = uninstall-blocked-before
        ]]
"

WORK="${WORKBASE}"

pkg_setup() {
    exdirectory --allow /opt
}

src_install() {
    # NPAPI version
    exeinto /opt/netscape/plugins
    doexe libflashplayer.so

    hereenvd 60flash <<EOF
MOZ_PLUGIN_PATH="/opt/netscape/plugins"
COLON_SEPARATED="MOZ_PLUGIN_PATH"
EOF

    # PPAPI version
    insinto /opt/chromium/PepperFlash
    doins {libpepflashplayer.so,manifest.json}

    insinto /etc/chromium
    hereins default  <<EOF
CHROMIUM_FLAGS="--ppapi-flash-path=/opt/chromium/PepperFlash/libpepflashplayer.so --ppapi-flash-version=${PV}"
EOF

    # flash-player-properties (NPAPI version only)
    exeinto /usr/$(exhost --target)/bin
    doexe usr/bin/flash-player-properties
    insinto /usr/share/applications
    doins usr/share/applications/flash-player-properties.desktop
    insinto /usr/share/icons/hicolor
    doins -r usr/share/icons/hicolor/*
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

