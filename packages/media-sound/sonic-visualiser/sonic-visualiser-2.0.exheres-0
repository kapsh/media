# Copyright 2013 Ali Polatel <alip@exherbo.org>
# Based in part upon sonic-visualiser-1.9.ebuild which is:
#   Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.13 ] ]
require freedesktop-desktop

SUMMARY="Application for viewing and analysing the contents of music audio files"
HOMEPAGE="http://www.sonicvisualiser.org"
DOWNLOADS="http://code.soundsoftware.ac.uk/attachments/download/539/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="fftw id3tag mp3 osc
    ( jack portaudio pulseaudio ) [[ number-selected = at-least-one ]]
"

# ogg requires fishsound and oggz which aren't available
DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/redland
        media-libs/dataquay
        media-libs/dssi
        media-libs/ladspa-sdk
        media-libs/liblrdf
        media-libs/libsndfile
        media-libs/libsamplerate
        media-libs/rubberband
        media-libs/speex
        media-libs/vamp-plugin-sdk[>=2.3]
        fftw? ( sci-libs/fftw )
        id3tag? ( media-libs/libid3tag )
        jack? ( media-sound/jack-audio-connection-kit )
        mp3? ( media-libs/libmad )
        osc? ( media-libs/liblo )
        portaudio? ( media-sound/portaudio )
        pulseaudio? ( media-sound/pulseaudio )
        x11-libs/qt:4
"

SV_SUBDIRS=( sonic-visualiser svapp svcore svgui )
sv_disable_option() {
    einfo "Disabling option ${1}"
    for subdir in "${SV_SUBDIRS[@]}"; do
        edo sed -i \
                -e "/${1}/d" "${WORK}/${subdir}/configure.ac"
    done
}

src_prepare() {
    default

    sv_disable_option fishsound # not available
    sv_disable_option oggz      # not available

    option fftw         || sv_disable_option fftw3f
    option fftw         || sv_disable_option fftw3
    option id3tag       || sv_disable_option id3tag
    option jack         || sv_disable_option jack
    option mp3          || sv_disable_option mad
    option osc          || sv_disable_option liblo
    option portaudio    || sv_disable_option portaudio
    option pulseaudio   || sv_disable_option pulseaudio

    for subdir in "${SV_SUBDIRS[@]}"; do
        edo pushd "${subdir}"
        eautoreconf
        edo popd
    done
}

src_compile() {
    EXJOBS=1 default
}

src_install() {
    # make: *** No rule to make target `install'.
    edo cd ${PN}
    dobin ${PN}
    emagicdocs
    # install samples
    insinto /usr/share/${PN}
    doins -r samples
    # desktop entry
    insinto /usr/share/pixmaps
    doins icons/sv-icon.svg
    insinto /usr/share/applications
    doins *.desktop
}

