# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require cmake \
     freedesktop-desktop \
     gtk-icon-cache

SUMMARY="An advanced raw photo development program"
DESCRIPTION="
Cross platform image processing software equipped with the essential tools for high quality and
efficient RAW photo development.
"
HOMEPAGE="https://rawtherapee.com"
DOWNLOADS="${HOMEPAGE}/shared/source/${PNV}.tar.xz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    openmp
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# bundles dcraw 9.28 and KLT 1.3.4
DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-cpp/cairomm:1.0
        dev-cpp/libsigc++:2[>=2.3.1]
        dev-libs/expat[>=2.1]
        dev-libs/glib:2[>=2.44]
        gnome-bindings/glibmm:2.4[>=2.44]
        gnome-bindings/gtkmm:3[>=3.16]
        gnome-desktop/librsvg:2[>=2.40]
        media-libs/lcms2[>=2.6]
        media-libs/lensfun[>=0.2]
        media-libs/libcanberra
        media-libs/libiptcdata
        media-libs/libpng:=
        media-libs/tiff[>=4.0.4]
        sci-libs/fftw
        sys-libs/zlib
        x11-libs/gtk+:3[>=3.16]
        openmp? ( sys-libs/libgomp:= )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-5.6-cmake.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DAPPDATADIR:PATH=/usr/share/metainfo
    -DAUTO_GDK_FLUSH:BOOL=FALSE
    -DCREDITSDIR:PATH=/usr/share/doc/${PNVR}
    -DDATADIR:PATH=/usr/share/${PN}
    -DDESKTOPDIR:PATH=/usr/share/applications
    -DDOCDIR:PATH=/usr/share/doc/${PNVR}
    -DENABLE_TCMALLOC:BOOL=FALSE
    -DICONSDIR:PATH=/usr/share/icons
    -DLICENCEDIR:PATH=/usr/share/doc/${PNVR}
    -DSTRICT_MUTEX:BOOL=TRUE
    -DTRACE_MYRWMUTEX:BOOL=FALSE
    -DWITH_BENCHMARK:BOOL=FALSE
    -DWITH_LTO:BOOL=FALSE
    -DWITH_MYFILE_MMAP:BOOL=TRUE
    -DWITH_PROF:BOOL=FALSE
    -DWITH_SAN:BOOL=FALSE
    -DWITH_SYSTEM_KLT:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'openmp OPTION_OMP'
)

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

