(
    media/kodi[~scm]
    media/mpv[~scm]
    media/vlc[~scm]
    media-libs/babl[~scm]
    media-libs/gegl[~scm]
    media-libs/libaacs[~scm]
    media-libs/libbluray[~scm]
    media-libs/libdvdnav[~scm]
    media-libs/libdvdread[~scm]
    media-libs/libmpdclient[~scm]
    media-libs/libmypaint[~scm]
    media-libs/libpostproc[~scm]
    media-libs/opus[~scm]
    media-libs/x264[~scm]
    media-libs/zxing-cpp[~scm]
    media-sound/mpc[~scm]
    media-sound/mpd[~scm]
    media-sound/ncmpcpp[~scm]
    media-sound/tomahawk[~scm]
    media-video/cinelerra[~scm]
    media-video/obs-studio[~scm]
) [[
    *author = [ Paul Seidler <sepek@exherbo.org> ]
    *date = [ 06 Dec 2011 ]
    *token = scm
    *description = [ Mask scm versions ]
]]

media-sound/jack-audio-connection-kit[>=1.9.0] [[
    author = [ Adriaan Leijnse <adriaan@leijnse.net> ]
    date = [ 20 Feb 2010 ]
    token = testing
    description = [ Mask testing version ]
]]

net-plugins/adobe-flash[<32.0.0.387] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Jun 2020 ]
    token = security
    description = [ APSB20-30 ]
]]

media-libs/taglib[<1.11.1-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Aug 2017 ]
    token = security
    description = [ CVE-2017-12678 ]
]]

media-libs/libraw[<0.19.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Feb 2019 ]
    token = security
    description = [ CVE-2018-20363, CVE-2018-20364, CVE-2018-20365 ]
]]

media-libs/libexif[<0.6.22] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Jul 2020 ]
    token = security
    description = [ CVE-2016-6328, CVE-2017-7544, CVE-2018-20030, CVE-2019-9278,
                    CVE-2020-0093, CVE-2020-12767, CVE-2020-13112, CVE-2020-13113,
                    CVE-2020-13114 ]
]]

media-gfx/gimp[<2.8.22-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 Jan 2018 ]
    token = security
    description = [ CVE-2017-1778{4,5,6,7,8,9} ]
]]

media-libs/OpenJPEG:0[<1.5.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 25 Aug 2014 ]
    token = security
    description = [ Multiple CVEs, see NEWS for details ]
]]

media-libs/lcms2[<2.8-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 25 Jan 2017 ]
    token = security
    description = [ CVE-2016-10165 ]
]]

media-libs/flac[<1.3.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Nov 2014 ]
    token = security
    description = [ CVE-2014-8962, CVE-2014-9028 ]
]]

graphics/exiv2[<0.27.2-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Oct 2019 ]
    token = security
    description = [ CVE-2019-17402 ]
]]

media/ffmpeg[<4.2.2-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 Apr 2020 ]
    token = security
    description = [ CVE-2020-12284 ]
]]

media-libs/libsndfile[<1.0.28-r1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 02 May 2017 ]
    token = security
    description = [ CVE-2017-{8361,8362,8363,8365} ]
]]

media-libs/giflib[<5.1.7] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 11 Mar 2019 ]
    token = security
    description = [ CVE-2018-11490 ]
]]

media-gfx/optipng[<0.7.6-r2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Dec 2017 ]
    token = security
    description = [ CVE-2017-16938 ]
]]

media-libs/imlib2[<1.4.9] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 17 May 2016 ]
    token = security
    description = [ CVE-2011-5326, CVE-2016-{3993,3994,4024} ]
]]

media/vlc[<3.0.11] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 17 Jun 2020 ]
    token = security
    description = [ CVE-2020-13428 ]
]]

media-libs/libass[<0.13.4] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 11 Oct 2016 ]
    token = security
    description = [ CVE-2016-7969, CVE-2016-7970, CVE-2016-7972 ]
]]

(
    media-plugins/gst-plugins-bad:0[<0.10.23-r3]
    media-plugins/gst-plugins-bad:1.0[<1.10.1]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 22 Nov 2016 ]
    *token = security
    *description = [ CVE-2016-944{5,6,7} ]
]]

media-libs/libwebp[<0.5.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Jan 2017 ]
    token = security
    description = [ CVE-2016-8888, CVE-2016-9085 ]
]]

media-libs/opus[<1.1.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Jan 2017 ]
    token = security
    description = [ CVE-2017-0381 ]
]]

media-libs/audiofile[<0.3.6-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Jan 2017 ]
    token = security
    description = [ CVE-2015-7747 ]
]]

media-gfx/icoutils[<0.31.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Mar 2017 ]
    token = security
    description = [ CVE-2017-60{09,10,11} ]
]]

media-gfx/sane-backends[<1.0.27] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2017-6318 ]
]]

media/kodi[<17.3-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Jul 2017 ]
    token = security
    description = [ CVE-2012-6706 ]
]]

media-sound/mpg123[<1.25.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 12 Jul 2017 ]
    token = security
    description = [ CVE-2017-11126 ]
]]

media-sound/vorbis-tools[<1.4.0-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 31 Jul 2017 ]
    token = security
    description = [ CVE-2014-9638, CVE-2014-9640 ]
]]

media-sound/lame[<3.100] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 18 Oct 2017 ]
    token = security
    description = [ CVE-2017-15045, CVE-2017-15046 ]
]]

media-libs/libvorbis[<1.3.6-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Dez 2019 ]
    token = security
    description = [ CVE-2017-14160, CVE-2018-10392, CVE-2018-10393 ]
]]

media/plex-media-server-bin[>1.13.5.5332] [[
    author = [ Rasmus Thomsen <cogitri@exherbo.org> ]
    date = [ 23 Aug 2018 ]
    token = testing
    description = [ Subscriber-only versions ]
]]

media/mpv[<0.27.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 10 May 2018 ]
    token = security
    description = [ CVE-2018-6360 ]
]]

media-libs/lensfun[~>0.3.95] [[
    author = [ Bernd Steinhauser <berniyh@exherbo.org> ]
    date = [ 21 Sep 2018 ]
    token = pre-release
    description = [ Pre-release version ]
]]

media-libs/live[<20181017] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Oct 2018 ]
    token = security
    description = [ CVE-2018-4013 ]
]]

media-video/mkvtoolnix[<28.2.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Oct 2018 ]
    token = security
    description = [ CVE-2018-4022 ]
]]

media-libs/SDL_image:2[<2.0.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Nov 2018 ]
    token = security
    description = [ CVE-2018-3977 ]
]]

media-libs/OpenJPEG:2[<2.3.1-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Feb 2020 ]
    token = security
    description = [ CVE-2020-8112 ]
]]

media-libs/libheif[<1.5.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Sep 2019 ]
    token = security
    description = [ CVE-2019-11471 ]
]]

media-libs/faad2[<2.9.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 Sep 2019 ]
    token = security
    description = [ CVE-2018-19502, CVE-2018-19503, CVE-2018-19504, CVE-2018-20194,
                    CVE-2018-20195, CVE-2018-20196, CVE-2018-20197, CVE-2018-20198,
                    CVE-2018-20199, CVE-2018-20357, CVE-2018-20358, CVE-2018-20359,
                    CVE-2018-20360, CVE-2018-20361, CVE-2018-20362, CVE-2019-6956,
                    CVE-2019-15296 ]
]]

media-libs/faac[<1.30] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Oct 2019 ]
    token = security
    description = [ CVE-2018-19886 ]
]]

media-libs/libmp4v2[<2.0.0-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 07 Nov 2019 ]
    token = security
    description = [ CVE-2018-14054, CVE-2018-14325, CVE-2018-14326, CVE-2018-14379,
                    CVE-2018-14403, CVE-2018-14446 ]
]]

media-gfx/jhead[<3.04] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 02 Sept 2020 ]
    token = security
    description = [ CVE-2019-19035, CVE-2019-1010301, CVE-2019-1010302 ]
]]
